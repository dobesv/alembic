-- ${message}
--
-- Revision ID: ${up_revision}
-- Revises: ${down_revision | comma,n}
-- Create Date: ${create_date}

SET @alembic_revision = ${repr(up_revision)};
SET @alembic_down_revision = ${repr(down_revision)};
SET @alembic_branch_labels = ${repr(branch_labels)};
SET @alembic_depends_on = ${repr(depends_on)};

SQL GOES HERE
