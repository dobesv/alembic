import os

from alembic.testing.env import staging_env, clear_staging_env
from alembic.testing.fixtures import TestBase
from alembic import util
from alembic.testing import eq_, assert_raises_message


class APITest(TestBase):
    def tearDown(self):
        clear_staging_env()

    def test_sql_rev_gen(self):
        env = staging_env(template='sql')
        abc = util.rev_id()
        script = env.generate_revision(abc, "this is a message", refresh=True)
        eq_(script.doc, "this is a message")
        eq_(script.revision, abc)
        eq_(script.down_revision, None)
        assert os.access(
            os.path.join(env.dir, 'versions',
                         '%s_this_is_a_message.sql' % abc), os.F_OK)
        assert callable(script.module.upgrade)
        eq_(env.get_heads(), [abc])
        eq_(env.get_base(), abc)

        def_ = util.rev_id()
        script = env.generate_revision(
            def_, "this is the next rev", refresh=True)
        assert os.access(
            os.path.join(
                env.dir, 'versions',
                '%s_this_is_the_next_rev.sql' % def_), os.F_OK)
        eq_(script.revision, def_)
        eq_(script.down_revision, abc)
        eq_(env.get_revision(abc).nextrev, set([def_]))
        assert script.module.down_revision == abc
        assert callable(script.module.upgrade)
        eq_(env.get_heads(), [def_])
        eq_(env.get_base(), abc)

        # Try creating a fresh env and see if it parses the generated file correctly
        env = staging_env(create=False)
        abc_rev = env.get_revision(abc)
        def_rev = env.get_revision(def_)
        eq_(abc_rev.nextrev, set([def_]))
        eq_(abc_rev.revision, abc)
        eq_(def_rev.down_revision, abc)
        eq_(env.get_heads(), [def_])
        eq_(env.get_base(), abc)
